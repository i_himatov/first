var sumA = 0;
var number = 1;

while(number <= 100){
	sumA+=number;
	number++;
}

console.log('sumA = ' + sumA);

var sumB = 0;

for (var number = 1; number <= 100; number++) {
	if ((number % 2 == 0) && (number % 3 == 0))
		sumB+=number;
}

console.log('sumB = ' + sumB);